package com.example.marcelojanke.myapplication.Web;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.marcelojanke.myapplication.Adapters.DesaparecidoAdapter;
import com.example.marcelojanke.myapplication.Objects.Pessoa;
import com.example.marcelojanke.myapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class WebInit {
    private final Context context;
    private String Url;

    private DesaparecidoAdapter adapter;

    public WebInit(Context context, String URL){
        this.context = context;
        this.Url = URL;
    }


    public DesaparecidoAdapter SendDesaparecidos(){
        adapter = new DesaparecidoAdapter(context,R.layout.adapterlayout);

        RequestQueue queue = Volley.newRequestQueue(this.context);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, this.Url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray object = new JSONArray(response);
                            for (int i = 0; i < object.length(); i++){
                                String current = (String) object.get(i);
                                String[] Arr = current.split("peitos");

                                adapter.add(new Pessoa(Arr[0],Arr[1],Arr[2],Integer.parseInt(Arr[3]),Arr[4]));
                            }
                        }catch (JSONException e){
                            Log.d("Naim", e.getMessage());
                        }
                    }
                },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,error.toString(),Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);

        return adapter;
    }

    public ArrayAdapter Info(String ID){

        RequestQueue queue = Volley.newRequestQueue(context);

        final ArrayAdapter<String> adapter= new ArrayAdapter<>(context,android.R.layout.simple_list_item_1);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, this.Url + "?idUser=" + ID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject object = new JSONObject(response.trim());
                            Iterator<String> keys = object.keys();

                            while(keys.hasNext()){
                                String key = keys.next();
                                adapter.add(key + " :: " + object.getString(key));

                                Log.d("Naim", object.getString(key));
                            }

                        } catch (JSONException e) {
                            Log.d("Naim", e.getMessage());
                        }
                    }
                },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,error.toString(),Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);


        return adapter;
    }


}
