package com.example.marcelojanke.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.marcelojanke.myapplication.Adapters.DesaparecidoAdapter;
import com.example.marcelojanke.myapplication.Objects.Pessoa;
import com.example.marcelojanke.myapplication.Web.WebInit;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView view = findViewById(R.id.Lista);
        final DesaparecidoAdapter adapter =
                new WebInit(this,"https://sheldonsumido.herokuapp.com/")
                        .SendDesaparecidos();

        view.setAdapter(adapter);

        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pessoa pessoa = adapter.getItem(position);
                assert pessoa != null;

                Intent i = new Intent(getApplicationContext(), PessoaPage.class);
                i.putExtra("ID",String.valueOf(pessoa.getID()));

                i.putExtra("Imagem",pessoa.getImagem());

                startActivity(i);
            }
        });


    }
}
