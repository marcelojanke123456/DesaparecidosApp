package com.example.marcelojanke.myapplication.Objects;

public class Pessoa  {
    private String nome,data,cidade,Imagem;
    private int ID;

    public Pessoa(String nome,String Data,String Cidade,int ID,String Imagem) {
        this.nome = nome;
        this.data = Data;
        this.cidade = Cidade;
        this.ID = ID;
        this.Imagem = Imagem;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNome() {
        return nome;
    }

    public String getImagem() {
        return Imagem;
    }

    public void setImagem(String imagem) {
        Imagem = imagem;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
}
