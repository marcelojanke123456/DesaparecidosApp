package com.example.marcelojanke.myapplication.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.marcelojanke.myapplication.Objects.Pessoa;
import com.example.marcelojanke.myapplication.R;
import com.squareup.picasso.Picasso;

public class DesaparecidoAdapter extends ArrayAdapter<Pessoa> {
    private int Resource;

    public DesaparecidoAdapter(Context context, int resource) {
        super(context, resource);
        this.Resource = resource;
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        @SuppressLint("ViewHolder")
        View view = LayoutInflater.from(getContext()).
                inflate(this.Resource,parent,false);

        Pessoa pessoa = getItem(position);

        TextView Nome = view.findViewById(R.id.Nome);
        TextView Data = view.findViewById(R.id.Data);
        TextView Local = view.findViewById(R.id.LocalDesaparecido);
        assert pessoa != null;

        String path = "https://sheldonsumido.herokuapp.com/imagens/" + pessoa.getImagem();

        ImageView imageView = view.findViewById(R.id.Foto);
        Picasso.get().load(path).into(imageView);

        Log.d("Image",path);


        Nome.setText(pessoa.getNome());
        Data.setText(pessoa.getData());
        Local.setText(pessoa.getCidade());



        return view;
    }
}
