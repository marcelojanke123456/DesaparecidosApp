package com.example.marcelojanke.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.marcelojanke.myapplication.Web.WebInit;
import com.squareup.picasso.Picasso;


public class PessoaPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pessoa_page);

        String id = getIntent().getStringExtra("ID");
        Toast.makeText(getApplicationContext(),id,Toast.LENGTH_SHORT).show();

        String path = "https://sheldonsumido.herokuapp.com/imagens/" + getIntent().getStringExtra("Imagem");

        ImageView view = findViewById(R.id.FotoPessoa);
        Picasso.get().load(path).into(view);

        ListView listView = findViewById(R.id.InfoPessoa);
        ArrayAdapter adapter = new WebInit(getApplicationContext(),"https://sheldonsumido.herokuapp.com/").Info(id);

        listView.setAdapter(adapter);

    }
}
